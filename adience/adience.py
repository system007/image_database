import re

from yahoo.yahoo import YAHOO

adience_root_path = 'D:/Documents/research/adience/'
adience_metadata_file = adience_root_path + 'all.txt'


def get_database(root_dir):

    with open(root_dir) as infile:

        next(infile)

        counter = 1
        counter_database = 1

        for line in infile:

            print('file counter', counter)
            counter += 1

            line_list = line.rstrip().split("\t")

            record_dict = {

                'user_id': line_list[0],
                'original_image': line_list[1],
                'face_id': line_list[2],
                'age': line_list[3],
                'gender': line_list[4],
                'x': line_list[5],
                'y': line_list[6],
                'dx': line_list[7],
                'dy': line_list[8],
                'tilt_ang': line_list[9],
                'fiducial_yaw_angle': line_list[10],
                'fiducial_score': line_list[11],
            }

            #print(record_dict)

            # check in yahoo database

            identifier = str(record_dict['original_image']).split('_', 1)[0].replace('.', '').upper()

            tags = [
                #'year',
                'birthday'
            ]

            tags_yahoo = [re.compile('.*{0}.*'.format(x), re.IGNORECASE) for x in tags ]

            # from database only 806 records where found on YAHOO CC

            yahoo_record = YAHOO()
            cursor = yahoo_record.get_data_for_adience(record_dict['user_id'], identifier, tags_yahoo)

            for x in cursor:
                print('database counter',counter_database)
                counter_database += 1
                print(x)

            # get from rest service



get_database(adience_metadata_file)