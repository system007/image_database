import smtplib
import keyring


class Email:
    def __init__(self, smtp_server, smtp_port, username, namespace):
        self.smtp_server = smtp_server
        self.smtp_port = smtp_port
        self.username = username
        self.namespace = namespace
        self.password = keyring.get_password(service_name=self.get_namespace(), username=self.get_username())

    def get_namespace(self):
        return self.namespace

    def get_username(self):
        return self.username

    def get_smtp_server(self):
        return self.smtp_server

    def get_smtp_port(self):
        return self.smtp_port

    def get_username(self):
        return self.username

    def get_password(self):
        return self.password

    def send(self, subject, text):

        smtp = self.get_smtp_server()
        port = self.get_smtp_port()

        server = smtplib.SMTP(smtp,port)
        server.ehlo()
        server.starttls()

        # Next, log in to the server
        server.login(self.get_username(), self.password)

        # Send the mail

        message = 'Subject: {}\n\n{}'.format(subject, text)

        server.sendmail(self.get_username(), self.get_username(), message)