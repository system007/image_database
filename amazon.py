import boto3
import configuration
from model import db_image, db_functions
from model.db_service import Service


class Amazon(Service):
    def __init__(self):
        Service.__init__(self, "SRV_AMAZON", 5000)

    def set_response(self, image_path):

        with open(image_path, "rb") as image_file:
            b = bytearray(image_file.read())

        client = boto3.client('rekognition')

        self.response = client.detect_faces(
            Image={
                'Bytes': b
            },

            Attributes=[
                'ALL'
            ]
        )

    def get_real_age_and_predicted(self, db_config, gender=None):
        """
Method overloaded so it can evaluate both images in a path or the whole dataset. This was extended to consider
Borderline cases
        :param db_config: databases
        :param age: optional parameter.If supplied, the age will be considered to compute over the entire estimated
        dataset per service
        :return: sorted dictionary
        """
        list_dictionary = []

        records = self.get_records_stats(gender)

        #list_test = []

        #for i in records:
        #    list_test.append(i['subject_id'])

        # print(len(list_test) != len(set(list_test)))

        for r in records:

            subject_id = r['subject_id']    # key

            data = db_image.get_data(db_config.get_db_list(), subject_id)

            real_age = db_functions.normalize_get_age(data)

            if 'FaceDetails' in r:
                face_details = r['FaceDetails']

                predicted_face_ages = []

                predicted_age = None

                for i in face_details:
                    age_range = i['AgeRange']
                    predicted_age = age_range['Low']
                    predicted_face_ages.append(predicted_age)

                if len(predicted_face_ages) > 0:
                    closest_value = min(predicted_face_ages, key=lambda x: abs(x - real_age)) #fsanda this for 2 faces. Change ASAP
                    predicted_age = closest_value

                if predicted_age is not None:

                    row = {
                        'subject_id': subject_id,
                        'real_age': real_age,
                        #'predicted_age_range': age_range,
                        'predicted_age': predicted_age,  # low is better for forensics
                        # 'predicted_age':  (age_range['Low'] + age_range['High'])/2,  # MAE increases
                        #'out_of_range': check_out_of_range(real_age, age_range),
                        #'closest_value': get_closest_value(real_age, age_range)
                    }

                    list_dictionary.append(row)

                else:
                    row = {
                        'subject_id': subject_id,
                        'real_age': real_age,
                        'predicted_age': 'N/A',  # low is better for forensics
                    }

                    if configuration.enable_csv_write_NA:
                        list_dictionary.append(row)

            elif 'Errors' in r or 'Error' in r:
                row = {
                    'subject_id': subject_id,
                    'real_age': real_age,
                    'predicted_age': "N/A",
                }

                # if false, it won't compute the N/A values!
                # True is useful for excel files

                if configuration.enable_csv_write_NA:
                    list_dictionary.append(row)

            else:
                print(r)

        '''
        list_test = []

        for i in list_dictionary:
            list_test.append(i['subject_id'])

        print(len(list_test) != len(set(list_test)))
        '''

        decorated = sorted(list_dictionary, key=lambda k: k['real_age'])

        return decorated

    def process_na_service(self):

        pipe = {
                '$or': [
                    {'Errors': {'$exists': True}},
                    {'error': {'$exists': True}},
                    {'FaceDetails': {'$exists': True, '$eq': []}}
                ]
            }

        return self.get_na_service(pipe)


def check_out_of_range(real_age, age_ranges):
    return not (age_ranges['Low'] <= real_age <= age_ranges['High'])


def get_closest_value(real_age, age_ranges):
    age_list = []

    for key, value in age_ranges.items():
        age_list.append(value)

    age_ranges['Mean'] = sum(age_list) / len(age_list)

    return min(age_ranges.items(), key=lambda x: abs(x[1] - real_age))

