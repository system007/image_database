import httplib, urllib, json
import time

import os

import how_old

from datetime import datetime

import configuration
from flickr import flickr_tools
from model import db_functions, db_image
from model.db_image import get_data
from model.db_service import Service


class Azure(Service):
    def __init__(self):
        Service.__init__(self, "SRV_MICROSOFT", 30000)
        self.subscription_key = configuration.azure_api_key

        self.exception = None

    def set_exception(self, exception):
        self.exception = exception

    def get_exception(self):
        return self.exception

    def get_subscription_key(self):
        return self.subscription_key

    def set_response(self, image_path):
        self.response = self._process_response(image_path)

    def _process_response(self, image_path):
        with open(image_path, "rb") as image_file:
            # b = bytearray(image_file.read())
            body = image_file.read()

        headers = {
            # Request headers
            'Content-Type': 'application/octet-stream',
            # 'Content-Type': 'application/json',
            'Ocp-Apim-Subscription-Key': self.get_subscription_key()
        }

        params = urllib.urlencode({
            # Request parameters

            'returnFaceId': 'true',
            'returnFaceLandmarks': 'true',
            'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise'

        })

        try:
            counter = 0

            while True or counter < 3:

                counter += 1

                conn = httplib.HTTPSConnection('westeurope.api.cognitive.microsoft.com')
                conn.request("POST", "/face/v1.0/detect?%s" % params, body, headers)
                response = conn.getresponse()

                data = response.read()
                conn.close()

                d = json.loads(data)

                if response.reason == "OK":

                    # check if empty

                    if len(d) == 0:
                        print('response empty...')

                        return {}

                        # return something

                    for i in d:
                        return i

                else:
                    if response.status == 429:

                        time_delay = 60

                        print(d)
                        print('Waiting for {0} seconds .....'.format(time_delay),  str(datetime.now()))
                        time.sleep(time_delay)
                    else:
                        print(image_path, d)

                        # todo resize image when too big and try again

                        #flickr_tools.convert_size()

                        return d

        except Exception as e:

            # TODO: try not to insert quota exceeded error but reattempt

            # if error.code = QuotaExceeded then do not insert record

            print(e)
            self.set_exception(e)
            return None

    def get_real_age_and_predicted(self, images, gender=None):

        list_dictionary = []

        records = self.get_records_stats(gender)

        for r in records:

            subject_id = r['subject_id']

            #data = cn_image.get_data(subject_id)
            data = db_image.get_data(images.get_db_list(), subject_id)

            real_age = db_functions.normalize_get_age(data)

            if 'faceAttributes' in r:
                predicted_age = r['faceAttributes']['age']

                row = {
                    'subject_id': subject_id,
                    'real_age': real_age,
                    'predicted_age': predicted_age
                }

                list_dictionary.append(row)

            else:
                row = {
                    'subject_id': subject_id,
                    'real_age': real_age,
                    'predicted_age': "N/A"
                }

                if configuration.enable_csv_write_NA:
                    list_dictionary.append(row)

        decorated = sorted(list_dictionary, key=lambda k: k['real_age'])

        return decorated

    def process_na_service(self):

        # consider empty responses too, for some reason this service returns an empty response.

        pipe = {'faceAttributes': {'$exists': False}}

        return self.get_na_service(pipe)

    def update_how_old_net(self, subject_id=None):

        if subject_id is None:

            pipe = {
                    "$and": [
                        {'faceAttributes': {'$exists': False}},
                        {'error': {'$exists': False}},
                        {'how-old': {'$exists': False}}
                    ]
                }

            records = self.get_db().find(
                pipe

            )

            print(records.count())

        else:
            records = self.get_db().find(
                {'subject_id': subject_id}
            )

        for r in records:

            # update record with how-old if possible

            object_id = r['subject_id']

            row = get_data(configuration.db_list, object_id)

            if row is not None and 'db' in row:

                database_object = row['db']

                database_object.set_image_path(row)

                full_path_url = database_object.manage_paths(row)

                if full_path_url is not None and os.path.exists(full_path_url):
                    with open(full_path_url, "rb") as image_file:
                        body = image_file.read()

                    how_old_response = how_old.web_process_response(body)

                    if how_old_response is not None:
                            if 'attributes' in how_old_response:
                                attributes = how_old_response['attributes']

                                age = attributes['age']
                                gender = attributes['gender']

                                self.get_db().update({'_id': r['_id']}, {"$set": {
                                    "faceAttributes.age": age,
                                    "faceAttributes.gender": gender
                                }
                                }, upsert=False)

                                print('updated from how-old.net')

                            elif 'how-old' in how_old_response:

                                self.get_db().update({'_id': r['_id']}, {"$set":
                                                     how_old_response}, upsert=False)

                                print('checked in how-old but no results')
                            else:
                                print('something went wrong')
                    else:
                        # insert flag in db

                        self.get_db().update({'_id': r['_id']}, {"$set": {
                            "how-old": True
                        }
                        }, upsert=False)

                else:
                    print(r)
                    print(row)
                    print('path error')

                    self.get_db().remove({'subject_id': object_id})

                    print('record deleted')

            else:
                print('retrieved record non extant', 'delete maybe')

                self.get_db().remove({'subject_id':  object_id})

                print('record deleted')

