import itertools
import time
from collections import defaultdict

import configuration    # used for temp directory

from controller import cn_image
from model import db_evaluation, fi_functions
from model.db_image import get_deleted_list


def run_service(service, db_list):

    temp_directory = configuration.temp_directory

    fi_functions.delete_folder(temp_directory)

    # get items already evaluated by service

    evaluation_list = db_evaluation.get_service_list(service)

    # get deleted items

    #deleted_list = cn_image.get_deleted_list()

    deleted_list = get_deleted_list()

    # merge both lists

    filtered_list = list( set( evaluation_list + deleted_list))

    for db in db_list:

        # get items not evaluated by service

        service_images = db.get_record_eval(filtered_list)

        fi_functions.create_folder(temp_directory)

        if True:

        #try:

            counter = 0

            for record in service_images:

                # time start

                time_start = time.clock()

                new_path = cn_image.change_name(db, temp_directory, record)

                if new_path is not None:
                    response = service.execute_prediction(new_path)
                    fi_functions.delete_file_physically(new_path)

                    if not response:
                        return False
                else:
                    print('path does not exist')

                # time stop

                time_stop = time.clock()

                print('time spent', time_stop-time_start)

                if service.get_limit() > 0:
                    counter += 1
                    print('service request', counter)

                    if counter == service.get_limit():
                        break

            service_images.close()

        # except Exception as ex:
        #    print(record, ex)

    return True


def execute_prediction(service, path):

    """
estimate age within the path specified for a determined service
    :param service: estimation service
    :param path: directory to execute prediction
    """
    print(service.get_name(), 'prediction')

    return service.execute_predictions(path)


def get_stats(service, path, db_list, gender=None):
    """
Public method to access statistics
    :param service:
    :param path:
    :param db_list:
    :param gender:
    :return:
    """
    return _get_stats(service, path, db_list, gender)


def _get_stats(service, path, db_list, gender=None):
    """

    :param service:
    :param path:
    :param db_list:
    :param gender:
    :return:
    """
    print('service stats', service.get_name())

    # gets _id of images in path

    images = cn_image.Query(db_list, path)

    service.set_path(path)

    list_predictions = service.get_real_age_and_predicted(images, gender)

    return list_predictions


def _get_stats_per_db(service, db):

    print('service stats', service.get_name())
    images = cn_image.Query(db)

    list_predictions = service.get_real_age_and_predicted(images)

    return list_predictions


def predict_services(path, list_services):

    """
Predict all services in the list of a given folder
    :param path: directory containing images
    :param list_services: list of estimation services
    """
    for service in list_services:
        execute_prediction(service, path)


def print_stats(path, db_list, list_services):
    for service in list_services:
        stats = _get_stats(service, path, db_list)

        for s in stats:
            print(s)


def get_plot_data(stats):

    mean_dict ={}

    for key, group in itertools.groupby(stats, lambda item: item["real_age"]):
        #value_list = [abs(int(item["predicted_age"])-key) for item in group]
        value_list = [int(item["predicted_age"]) for item in group]
        mean_dict[key] = (sum(value_list)) / len(value_list)

    # group dictionary and get average

    return mean_dict


def get_box_plot(path, db_list, service, gender=None):

    # if gender isn't specified, the results will contemplate both male and female subjects

    stats = _get_stats(service, path, db_list, gender)

    v = {}

    for key, group in itertools.groupby(stats, lambda item: item["real_age"]):
        value_list = [item['predicted_age'] for item in group]
        v[key] = value_list

    return v


def get_plot_data_mae_intervals(path, db_list, service, gender=None):
    stats = _get_stats(service, path, db_list, gender)

    mae_dict ={}

    for key, group in itertools.groupby(stats, lambda item: item["real_age"]):
        value_list = [abs(float(item["predicted_age"]) - float(key)) for item in group]
        mae_dict[key] = (sum(value_list)) / len(value_list)

    grouped_dictionary = add_interval(mae_dict, 10)

    return grouped_dictionary


def add_interval(mae_dictionary, number):

    group_dictionary = {}
    counter = 1
    group_counter = 0
    value_list = []

    for key in sorted(mae_dictionary.iterkeys()):

        value_list.append(mae_dictionary[key])

        if counter % number != 0:
            counter += 1
        else:
            group_dictionary[group_counter] = [] + value_list
            group_counter += 1
            del value_list[:]
            counter = 1

    if group_counter not in group_dictionary.keys():
        group_dictionary[group_counter] = value_list

    return group_dictionary


def mean_interval(mae_dictionary, number):

    group_dictionary = {}
    counter = 1
    group_counter = 0
    avg_mae = 0

    for key in sorted(mae_dictionary.iterkeys()):

        avg_mae += mae_dictionary[key]

        if counter % number != 0:
            counter += 1
        else:
            group_dictionary[group_counter] = avg_mae/(number * 1.0)
            group_counter += 1
            avg_mae = 0
            counter = 1

    if group_counter not in group_dictionary.keys():
        group_dictionary[group_counter] = avg_mae/((counter-1) * 1.0)

    return group_dictionary


def get_plot_data_mae(path, db_list, service, gender=None):
    stats = _get_stats(service, path, db_list, gender)

    mae_dict ={}

    for key, group in itertools.groupby(stats, lambda item: item["real_age"]):
        value_list = [abs(float(item["predicted_age"]) - float(key)) for item in group]
        mae_dict[key] = (sum(value_list)) / len(value_list)

    return mae_dict


def calculate_mae(stats):

    sum_value = 0

    for i in stats:
        sum_value += abs(float(i['real_age'] - i['predicted_age']))

    mae = sum_value / len(stats)

    return mae


def mae(pred_list, true_list):

    if len(pred_list) != len(true_list):
        raise Exception('Error: number of elements match nicht!')
    return sum(
        map(lambda t: abs(float(t[0]-t[1])), zip(pred_list, true_list))
    )/len(true_list)