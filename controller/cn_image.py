import os
import random
import shutil

from bson import ObjectId

from feret.feret import FERET, process_bz2_feret
from model import fi_functions, db_functions, db_image
from model.db_image import get_na_list, get_deleted_list

age_range = 1


class Query:
    def __init__(self, db_list, path_directory_creation):

        self.db_list = db_list
        self.path_directory_creation = path_directory_creation
        self.number_files = None
        self.minimum_age = None
        self.maximum_age = None
        self.gender = None

    def set_number_files(self, number_file):
        self.number_files = number_file

    def get_number_files(self):
        return self.number_files

    def set_gender(self, gender):
        self.gender = gender

    def get_gender(self):
        return self.gender

    def set_minimum_age(self, minimum_age):
        self.minimum_age = minimum_age

    def get_minimum_age(self):
        return self.minimum_age

    def set_maximum_age(self, maximum_age):
        self.maximum_age = maximum_age

    def get_maximum_age(self):
        return self.maximum_age

    def get_db_list(self):
        return self.db_list

    def set_db_list(self, db_list):
        self.db_list = db_list

    def set_path_directory_creation(self, path_directory_creation):
        self.path_directory_creation = path_directory_creation

    def get_path_directory_creation(self):
        return self.path_directory_creation

    def check_max_file_set(self, min_value, max_value):
        db_functions.check_max_file_set(min_value, max_value, self.get_db_list())

    def process(self, path=None):

        #if path is not None:

        record_list = []

        # define databases

        db_list = self.get_db_list()

        database_dictionary = {}

        dictionary_filter = {}

        print('Retrieving filtered data...')

        # get deleted images

        deleted_list = get_deleted_list()
        na_list = get_na_list()

        filtered_list = list(set(deleted_list + na_list))

        for db in db_list:
            dictionary_filter[db] = db.get_filtered_images(filtered_list)

       # once = False

        for i in range(self.get_minimum_age(), self.get_maximum_age() + age_range):

            # 1) create folders

            directory = self.get_path_directory_creation() + str(i) + "/" + self.get_gender() + "/"

            if not os.path.exists(directory):
                fi_functions.create_folder(directory)

            # get active database, not all databases have certain ages

            # according to other databases we will use female 0 and male 1

            #binary_gender = 0 if self.get_gender() == 'Female' else 1

            if str(self.get_gender()).lower() == 'female':
                binary_gender = 0
            elif str(self.get_gender()).lower() == 'male':
                binary_gender = 1
            else:
                print('Please check gender value')
                return None

            #if not once:

            # get na_list and deleted list

            for d in db_list:

                filtered_images = dictionary_filter[d]

                database_dictionary[d] = d.count_images(i, binary_gender, filtered_images)
            #else:
            #    once = True

            active_databases = []

            sum_images = 0

            print('age', i)

            for key, value in database_dictionary.items():
                sum_images += value

                if value > 0:
                    active_databases.append(key)
                    print(key.get_name(), value)

            print('sum images =' + str(sum_images))

            if sum_images < self.get_number_files():
                print('Sorry not enough images')
                return None

            count_images = 0

            while count_images != self.get_number_files():
                if len(active_databases) > 0:
                    random_db_class = random.choice(active_databases)
                else:
                    print('no active databases')
                    break

                # get data according to db

                record = random_db_class.get_random_image(i, binary_gender, filtered_images)

                random_db = random_db_class.get_name()

                if record is not None:
                    new_path = change_name(random_db_class, directory, record)

                    if new_path is not None:
                        database_dictionary[random_db_class] -= 1
                        count_images += 1
                        record_list.append(record)

                else:
                    print('No more random files available', random_db)
                    active_databases.remove(random_db_class)

            once = True

        return record_list

    def delete_random_databases(self):

        for i in self.get_db_list():
            i.delete_random_documents()

    def get_data(self, record_id):
        return db_image.get_data(self.get_db_list(), record_id)

    def get_image(self, record_id):
        row = self.get_data(record_id)

        database_object = row['db']

        file_name = row['full_path']

        full_path_url = database_object.manage_paths(file_name)

        if full_path_url is not None:
            count = os.stat(full_path_url).st_size / 2
            print(count)
            with open(full_path_url, "rb") as f:
                return bytearray(f.read())

    def get_age(self, file_name):
        record_id = os.path.splitext(os.path.basename(file_name))[0]
        record = db_image.get_data(self.get_db_list(), record_id)
        age = db_functions.normalize_get_age(record)
        return age

    def replace_image(self, text):
        print(text)

        record_id = os.path.splitext(os.path.basename(text))[0]

        delete_record(record_id)

        fi_functions.delete_file_physically(text)

        record = self.get_data(record_id)

        age = db_functions.normalize_get_age(record)
        gender = db_functions.normalize_get_gender(record)

        # generate again new image

        self.set_minimum_age(age)
        self.set_maximum_age(age)
        self.set_gender(gender)
        self.set_number_files(1)

        run_process = self.process()

        if run_process is not None:

            if len(run_process) == 1:

                for p in run_process:
                    image_path = p['new_path']

                print(image_path)

                return image_path

    def delete_folder(self):
        fi_functions.delete_folder(self.get_path_directory_creation())

    def save_database(self):
        # iterate folder
        list_files = fi_functions.iterate_folder(self.get_path_directory_creation())
        # send items to db_image
        db_image.save_dataset(
            list_files,
            self.get_gender(),
            self.get_minimum_age(),
            self.get_maximum_age(),
            self.get_number_files()
        )


def delete_record(object_id):
    db_image.delete_file(object_id)


def change_name(random_db_class, directory, record):

        random_db_class.set_image_path(record)

        image_path = random_db_class.get_image_path()

        # check if image_path exists

        if image_path is not None:

            if os.path.exists(image_path):

                shutil.copy2(image_path, directory)

                if isinstance(random_db_class, FERET):
                    file_name = process_bz2_feret(image_path, directory)
                else:
                    file_name = os.path.basename(image_path)

                new_path = fi_functions.rename_file(record, directory, file_name, random_db_class)

                record['new_path'] = new_path

                return new_path

            else:
                print("image file is not None but doesn't exist")
                # therefore, delete record
                delete_record(ObjectId(record['_id']))
                print("therefore, record deleted")


#def get_deleted_list():

#    db = Database("delete")

    #list_deleted = db.get_deleted_list()

    #return list_deleted
