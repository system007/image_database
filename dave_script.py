# IMPORTANT: Set True in KAIROS & AMAZON TO CREATE ENTIRE FILE

#get real age, predicted age for all items.

#subject_id, database, real_age, real_gender, amazon_age, amazon_gender, microsoft_age, microsoft_gender, kairos_age, kairos_gender, dex_age, dex_gender
from pymongo import MongoClient

import configuration
from DEX import Dex
from amazon import Amazon
from azure import Azure
from controller.cn_evaluation import get_stats
from kairos import Kairos
from model import db_functions
import csv

db_list = configuration.db_list

list_services = configuration.list_services


def get_real_age_and_predicted_experiment(path):

    for service in list_services:

        counter = 0

        csv_filename = 'real_age_and_predicted_' + service.get_name() + '_.csv'

        with open(csv_filename, 'wb') as file_test:  # Just use 'w' mode in 3.x

            stats = get_stats(service, path, db_list)

            for rec in stats:
                #subject_id = rec['subject_id']
                #real_age = rec['real_age']
                #predicted_age = ['predicted_age']

                w = csv.DictWriter(file_test, rec.keys())

                if counter == 0:
                    w.writeheader()
                w.writerow(rec)

                counter += 1

    # get real_age

    # get kairos_prediction

    # get amazon_prediction

    # get dex_prediction

    # get microsoft_prediction


def get_real_age_and_predicted(database, records):
    list_dictionary = []

    counter = 0

    csv_filename = database.get_name() + '_TEST.csv'

    with open(csv_filename, 'wb') as file_test:  # Just use 'w' mode in 3.x

        for rec in records:

            subject_id = rec['_id']

            real_age = db_functions.normalize_get_age(rec)

            # get prediction

            for s in list_services:

                r = s.get_db().find_one(
                    {
                        'subject_id': subject_id
                    }
                )

                #check type of service

                not_assigned = True

                if isinstance(s, Azure):
                    if r is not None and 'faceAttributes' in r:
                        predicted_age = r['faceAttributes']['age']

                        row = {
                            'subject_id': subject_id,
                            'real_age': real_age,
                            'predicted_age': predicted_age
                        }

                        list_dictionary.append(row)
                        not_assigned = False
                elif isinstance(s, Amazon):
                    if r is not None and 'FaceDetails' in r:
                        face_details = r['FaceDetails']

                        for i in face_details:
                            age_range = i['AgeRange']

                            row = {
                                'subject_id': subject_id,
                                'real_age': real_age,
                                #'predicted_age_range': age_range,
                                'predicted_age': age_range['Low'],  # low is better for forensics
                            }

                        not_assigned = False

                            #list_dictionary.append(row)

                    elif r is not None and 'Errors' in r:
                        not_assigned = True

                elif isinstance(s, Kairos):

                    if r is not None and 'images' in r:
                        img = r['images']
                        for i in img:
                            faces = i['faces']

                            for f in faces:
                                attributes = f['attributes']

                                row = {
                                    'subject_id': subject_id,
                                    'real_age': real_age,
                                    'predicted_age': attributes['age']
                                }

                                #list_dictionary.append(row)

                        not_assigned = False

                elif isinstance(s, Dex):
                    if r is None or 'age' not in r:
                        print('error', r)
                        not_assigned = True
                    else:
                        row = {
                            'subject_id': subject_id,
                            'real_age': real_age,
                            'predicted_age': r['age']
                        }

                        #list_dictionary.append(row)

                        not_assigned = False

                if not_assigned:
                    row = {
                        'subject_id': subject_id,
                        'real_age': real_age,
                        'predicted_age': "N/A"
                    }

                row.update({'db': s.get_name()})

                print(row)

                w = csv.DictWriter(file_test, row.keys())

                if counter == 0:
                    w.writeheader()
                w.writerow(row)

                counter +=1

                # print to csv file


cl = MongoClient()

#deleted_list = cn_image.get_deleted_list()
deleted_list = None

if False:
    for db in configuration.db_list:
        rec = db.get_record_eval(deleted_list)
        get_real_age_and_predicted(db, rec)

get_real_age_and_predicted_experiment(configuration.path_directory_creation)

print('done')