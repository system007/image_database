import os
import random

from model.db_image import Database, get_na_list


class FLICKR(Database):
    def __init__(self):
        Database.__init__(self, "FLICKR")

    def set_image_path(self, record):
        file_name = record['id_file'] + '.JPG'

        #include gender + age

        age = record['age']
        gender_record = record['gender']

        gender = ''

        if gender_record == 'F':
            gender = 'Female'
        elif gender_record == 'M':
            gender = 'Male'
        else:
            print('Error with gender values in database')

        directory = os.path.join(os.path.join(self.get_root_path(), gender), age)

        image_path = os.path.join(directory, file_name)
        self.image_path = image_path

    def upload_database(self, root_dir):

        for root, subFolders, files in os.walk(root_dir, topdown=True):
            for f in files:
                if f.lower().endswith(".jpg"):
                    id_file = os.path.splitext(f)[0]
                    #id_subject = get_code(f)

                    gender = get_gender(root)
                    age = get_age(root)

                    self.get_db().insert({
                        'id_file': id_file,
                        #'id_subject': id_subject,
                        'gender': gender,
                        'age': age
                    })

                    print('record inserted')

    def get_filtered_images(self, filtered_list):
        #list_deleted = self.get_deleted_list()
        list_processed = self.get_processed_list()
        #list_na_service = get_na_list()

        #return list(set(list_deleted + list_processed + list_na_service))

        return list(set(list_processed + filtered_list))

    def get_record(self, age, gender, filtered_images):
        gender_normalize = normalize_gender(gender)

        if gender_normalize is None:
            return gender_normalize

        # list_deleted = self.get_deleted_list()
        # list_processed = self.get_processed_list()
        # list_na_service = get_na_list()

        # filtered_list = list_deleted + list_processed + list_na_service

        images = self.get_db().find(
            {
                'age': str(age),
                'gender': gender_normalize,
                #'_id': {"$nin": filtered_list}
                '_id': {"$nin": filtered_images}
            }
        )

        return images

    def count_images(self, age, gender, filtered_images):
        images = self.get_record(age, gender, filtered_images)
        return images.count()

    def get_random_image(self, age, gender, filtered_images):

        images = self.get_record(age, gender, filtered_images)

        images_dictionary = []

        for row in images:
            images_dictionary.append(row)

        if len(images_dictionary) > 0:
            random_image = random.choice(images_dictionary)

            id_random = random_image['_id']

            if True:

                self.get_random().insert(
                    {'id': id_random}
                )

            return random_image

    def manage_paths(self, record):
        file_name = record['id_file'] + '.JPG'

        # include gender + age

        age = record['age']
        gender_record = record['gender']

        gender = ''

        if gender_record == 'F':
            gender = 'Female'
        elif gender_record == 'M':
            gender = 'Male'
        else:
            print('Error with gender values in database')

        directory = os.path.join(os.path.join(self.get_root_path(), gender), age)

        image_path = os.path.join(directory, file_name)

        return image_path


def get_code(file):
    code = file[:6]
    return code


def get_gender(directory):
    # get directory

    gender = os.path.basename(os.path.abspath(os.path.join(directory, os.pardir)))

    return gender[:1]


def get_age(directory):
    age = os.path.basename(directory)
    return age


def normalize_gender(gender):
    if gender == 0:
        return 'F'
    elif gender == 1:
        return 'M'
    else:
        print('Error normalizing gender')