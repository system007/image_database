import os
import math


def convert_size(size_bytes):
   if size_bytes == 0:
       return "0B"
   size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
   i = int(math.floor(math.log(size_bytes, 1024)))
   p = math.pow(1024, i)
   s = round(size_bytes / p, 2)
   return "%s %s" % (s, size_name[i])


#root_dir = 'D:/Documents/research/evaluation/dataset/'
#root_dir = 'D:/Documents/research/evaluation/flickr_manual_images/New folder/'

root_dir = 'D:/Documents/research/evaluation/dataset/'

for root, subFolders, files in os.walk(root_dir, topdown=True):
    for f in files:
        if f.lower().endswith(".jpg"):

            fil,ext = os.path.splitext(f)
            new_file_path = os.path.join(root, fil+'_old' + ext)
            file_path = os.path.join(root, f)
            file_size = os.path.getsize(file_path)

            if not file_size < ( 1024 * (4 * 1024)):

                from PIL import Image, ImageFile

                resize_image = Image.open(file_path)

                #resize_image = resize_image.resize((resize_image.size[0], resize_image.size[1]), Image.ANTIALIAS)

                #ImageFile.MAXBLOCK = max(resize_image.size) ** 2

                #ImageFile.MAXBLOCK = 2**20

                resize_image.save(file_path, quality=20)

                print('file shrinked', file_path)

                resize_image.close()