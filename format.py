import scipy.io
import numpy as np

filepath = 'D:\Download\data-vision\imdb\imdb.mat'

data = scipy.io.loadmat(filepath)

for i in data:
	if '__' not in i and 'readme' not in i:
		print(data[i])
		np.savetxt((i+".csv"),data[i],delimiter=',', fmt='%s')
