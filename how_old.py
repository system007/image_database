import json
import requests

import httplib, urllib, json
import time

import os

import how_old

from datetime import datetime

import configuration
from model import db_functions, db_image
from model.db_image import get_data
from model.db_service import Service


class HowOld(Service):
    def __init__(self):
        Service.__init__(self, "SRV_HOW-OLD", -1)
        self.exception = None

    def set_exception(self, exception):
        self.exception = exception

    def get_exception(self):
        return self.exception

    def set_response(self, image_path):
        self.response = self._process_response(image_path)

    def _process_response(self, image_path):
        with open(image_path, "rb") as image_file:
            # b = bytearray(image_file.read())
            body = image_file.read()

        headers = {
            # Request headers
            'Content-Type': 'application/octet-stream',
            'Cache-Control': 'no-cache',
            "Connection": "keep-alive",
            "Origin": "https://www.mywbsite.fr",
            "X-Requested-With": "XMLHttpRequest",
            "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.52 Safari/536.5",
            "Accept": "*/*",
            "Accept-Encoding": "gzip,deflate,sdch",
            "Accept-Language": "fr-FR,fr;q=0.8,en-US;q=0.6,en;q=0.4",
            "Accept-Charset": "ISO-8859-1,utf-8;q=0.7,*;q=0.3",
        }

        try:

            response = requests.post(
                url="https://how-old.net/Home/Analyze?isTest=False&source=&version=how-old.net HTTP/1.1",
                headers=headers, data=body, verify=False)

            if response.status_code == 200:

                data = json.loads(json.loads(response.text))

                faces = data.get('Faces')

                if faces:
                    for face in faces:
                        attrs = face['attributes']
                        age = attrs['age']
                        gender = attrs['gender']
                        # TODO: one face only at the moment and no landmarks

                        return face
                else:
                    print('No faces')
                    return {
                        'Error': 'No faces'
                    }

        except Exception as e:
            print(e)
            return {
                'Error': e
            }

    def get_real_age_and_predicted(self, images, gender=None):

        list_dictionary = []

        records = self.get_records_stats(gender)

        for r in records:

            subject_id = r['subject_id']

            #data = cn_image.get_data(subject_id)
            data = db_image.get_data(images.get_db_list(), subject_id)

            real_age = db_functions.normalize_get_age(data)

            if 'attributes' in r:
                predicted_age = r['attributes']['age']

                row = {
                    'subject_id': subject_id,
                    'real_age': real_age,
                    'predicted_age': predicted_age
                }

                list_dictionary.append(row)

            else:
                row = {
                    'subject_id': subject_id,
                    'real_age': real_age,
                    'predicted_age': "N/A"
                }

                if configuration.enable_csv_write_NA:
                    list_dictionary.append(row)

        decorated = sorted(list_dictionary, key=lambda k: k['real_age'])

        return decorated

    def process_na_service(self):

        # consider empty responses too, for some reason this service returns an empty response.

        pipe = {'faceAttributes': {'$exists': False}}

        return self.get_na_service(pipe)

    def update_how_old_net(self, subject_id=None):

        if subject_id is None:

            pipe = {
                    "$and": [
                        {'faceAttributes': {'$exists': False}},
                        {'error': {'$exists': False}},
                        {'how-old': {'$exists': False}}
                    ]
                }

            records = self.get_db().find(
                pipe

            )

            print(records.count())

        else:
            records = self.get_db().find(
                {'subject_id': subject_id}
            )

        for r in records:

            # update record with how-old if possible

            object_id = r['subject_id']

            row = get_data(configuration.db_list, object_id)

            if row is not None and 'db' in row:

                database_object = row['db']

                database_object.set_image_path(row)

                full_path_url = database_object.manage_paths(row)

                if full_path_url is not None and os.path.exists(full_path_url):
                    with open(full_path_url, "rb") as image_file:
                        body = image_file.read()

                    how_old_response = how_old.web_process_response(body)

                    if how_old_response is not None:
                            if 'attributes' in how_old_response:
                                attributes = how_old_response['attributes']

                                age = attributes['age']
                                gender = attributes['gender']

                                self.get_db().update({'_id': r['_id']}, {"$set": {
                                    "faceAttributes.age": age,
                                    "faceAttributes.gender": gender
                                }
                                }, upsert=False)

                                print('updated from how-old.net')

                            elif 'how-old' in how_old_response:

                                self.get_db().update({'_id': r['_id']}, {"$set":
                                                     how_old_response}, upsert=False)

                                print('checked in how-old but no results')
                            else:
                                print('something went wrong')
                    else:
                        # insert flag in db

                        self.get_db().update({'_id': r['_id']}, {"$set": {
                            "how-old": True
                        }
                        }, upsert=False)

                else:
                    print(r)
                    print(row)
                    print('path error')

                    self.get_db().remove({'subject_id': object_id})

                    print('record deleted')

            else:
                print('retrieved record non extant', 'delete maybe')

                self.get_db().remove({'subject_id':  object_id})

                print('record deleted')





def web_process_response(body):
    #with open(image_path, "rb") as image_file:
        # b = bytearray(image_file.read())
        #body = image_file.read()

    headers = {
        # Request headers
        'Content-Type': 'application/octet-stream',
        # 'Content-Type': 'application/json',
        #'Ocp-Apim-Subscription-Key': self.get_subscription_key()
        'Cache-Control': 'no-cache',
        #"Host": "www.mywbsite.fr",
        "Connection": "keep-alive",
        #"Content-Length": 129,
        "Origin": "https://www.mywbsite.fr",
        "X-Requested-With": "XMLHttpRequest",
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.52 Safari/536.5",
        #"Content-Type": "application/json",
        "Accept": "*/*",
        #"Referer": "https://www.mywbsite.fr/data/mult.aspx",
        "Accept-Encoding": "gzip,deflate,sdch",
        "Accept-Language": "fr-FR,fr;q=0.8,en-US;q=0.6,en;q=0.4",
        "Accept-Charset": "ISO-8859-1,utf-8;q=0.7,*;q=0.3",
        #"Cookie": "ASP.NET_SessionId=j1r1b2a2v2w245; GSFV=FirstVisit=; GSRef=https://www.google.fr/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&ved=0CHgQFjAA&url=https://www.mywbsite.fr/&ei=FZq_T4abNcak0QWZ0vnWCg&usg=AFQjCNHq90dwj5RiEfr1Pw; HelpRotatorCookie=HelpLayerWasSeen=0; NSC_GSPOUGS!TTM=ffffffff09f4f58455e445a4a423660; GS=Site=frfr; __utma=1.219229010.1337956889.1337956889.1337958824.2; __utmb=1.1.10.1337958824; __utmc=1; __utmz=1.1337956889.1.1.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided)"

    }

    try:

        response = requests.post(url="https://how-old.net/Home/Analyze?isTest=False&source=&version=how-old.net HTTP/1.1", headers=headers, data=body,  verify=False)

        if response.status_code == 200:

            data = json.loads(json.loads(response.text))

            faces = data.get('Faces')

            if faces:
                for face in faces:
                    attrs = face['attributes']
                    age = attrs['age']
                    gender = attrs['gender']
                    # TODO: one face only at the moment and no landmarks

                    return face
            else:
                print('No faces')
                return {
                    'how-old':True,
                }

    except Exception as e:
        print(e)
        return None

#image_path = 'D:/Documents/research/evaluation/dataset/13/Female/5952d1148ad9561f40c24ce5.jpg'

#web_process_response(image_path)