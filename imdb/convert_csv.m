I created a script using octave that can convert the structure of the DEX mat file to CSV so I can consume it
----------------------------------------------------------------------------------------------------------
1) load mat file to octave

load('D:\Documents\research\imdb\imdb_meta\imdb\imdb.mat')

----------------------------------------------------------------------------------------------------------
l = length(imdb.full_path)

try

  fid = fopen ('imdb.csv', 'a');

  fprintf(fid, '%s,%s,%s,%s,%s,%s,%s\n', 'full_path', 'name', 'age', 'gender', 'face_score', 'second_face_score', 'celeb_id')

  for i=1:l

    full_path = imdb.full_path(i){1}
    dob = imdb.dob(i)
    [age,~]=datevec(datenum(imdb.photo_taken(i),7,1)-imdb.dob(i));
    gender = imdb.gender(i)
    name = imdb.name(i){1}
    face_score = imdb.face_score(i)
    second_face_score = imdb.second_face_score(i)
    celeb_id  = imdb.celeb_id(i)

    fprintf(fid, '%s,%s,%d,%d,%d,%d,%d\n', full_path, name, age, gender, face_score, second_face_score, celeb_id);

  end
  fclose(fid)
catch ex
  disp(ex)
  disp(i)
  fclose(fid)
end