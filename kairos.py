import base64
import kairos_face

import configuration
from model import db_image, db_functions
from model.db_service import Service

kairos_face.settings.app_id = '1713d94a'
kairos_face.settings.app_key = 'e8cca5a46aa2e649f3b58b5372725247'

#kairos_face.settings.app_id = '46a8eb3b'
#kairos_face.settings.app_key = 'bff46138c92fd936cbf6a328abb3aa91'

#cfbd1747   (cap 1500)
#62b6b66007f8a7995fa550e8a55bfc30


class Kairos(Service):
    def __init__(self):
        Service.__init__(self, "SRV_KAIROS", -1)

    def set_response(self, image_path):
        with open(image_path, "rb") as image_file:
            encoded_string = base64.b64encode(image_file.read())

        self.response = kairos_face.detect_face(url=encoded_string)

    def get_real_age_and_predicted(self, images, gender=None):

        list_dictionary = []

        # get only saved elements

        records = self.get_records_stats(gender)

        for r in records:

            subject_id = r['subject_id']

            data = db_image.get_data(images.get_db_list(), subject_id)

            if data is None:
                print('check database configurations')
            else:
                real_age = db_functions.normalize_get_age(data)

                if 'images' in r:
                    img = r['images']

                    predicted_face_ages = []

                    for i in img:
                        faces = i['faces']

                        for f in faces:
                            attributes = f['attributes']

                            row = {
                                'subject_id': subject_id,
                                'real_age': real_age,
                                'predicted_age': attributes['age']
                            }

                            predicted_face_ages.append(attributes['age'])

                        if len(predicted_face_ages) > 0:
                            closest_value = min(predicted_face_ages,
                                                key=lambda x: abs(x - real_age))  # TODO: fsanda this for 2 faces. Change ASAP
                            row.update({'predicted_age': closest_value})

                        list_dictionary.append(row)     # TODO: same principal remove double faces

                elif 'Errors' in r or 'error' in r:
                    #print('Not Available')
                    row = {
                        'subject_id': subject_id,
                        'real_age': real_age,
                        'predicted_age': "N/A"
                    }

                    # if false, it won't compute the N/A values!
                    # True is useful for excel files

                    if configuration.enable_csv_write_NA:
                        list_dictionary.append(row)

                else:
                    print(r)
                    pass

        decorated = sorted(list_dictionary, key=lambda k: k['real_age'])

        return decorated

    def process_na_service(self):

        pipe = {
              '$or': [{'Errors': {'$exists': True}}, {'error': {'$exists': True}}]
            }

        return self.get_na_service(pipe)
