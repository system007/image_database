from datetime import datetime

from pymongo import MongoClient
from bson.objectid import ObjectId

import configuration

cl = MongoClient()
cl_deleted = cl["DELETE"]["data"]
cl_saved = cl["SAVE"]["data"]


class Database:

    def __init__(self, name):

        self.name = name
        self.root_path = ''
        self.crop_path = ''
        self.db = cl[name]["data"]
        self.r = cl[name]["random"]
        self.deleted = cl_deleted
        self.image_path = None
        self.filter_list = []

    def set_filter_list(self, filter_list):
        self.filter_list = filter_list

    def get_filter_list(self):
        return self.filter_list

    def get_image_path(self):
        print(self.image_path, self.get_name())  # is useful for debugging
        return self.image_path

    def set_root_path(self, path):
        self.root_path = path

    def set_crop_path(self, path):
        self.crop_path = path

    def get_name(self):
        return self.name

    def get_root_path(self):
        return self.root_path

    def get_crop_path(self):
        return self.crop_path

    def get_db(self):
        return self.db

    def get_random(self):
        return self.r

    def get_delete(self):
        return self.deleted

    def delete_random_documents(self):
        self.get_random().drop()

    def delete_file(self, object_id):
        self.get_delete().insert(
            {'id': ObjectId(object_id)}
        )

    def get_processed_list(self):
        list_processed = []
        processed = self.get_random().find({}, {"id": 1})

        for i in processed:
            list_processed.append(i['id'])

        return list_processed

    def get_record_eval(self, list_filtered):

        cursor = self.get_db().find(
            {
                '_id': {"$nin": list_filtered}
                # not in kairos, amazon, etc and not deleted
            }#, no_cursor_timeout=True
            #, timeout=False
        ).batch_size(10)
        return cursor

    def manage_paths(self, record):
        """
Manages the path output due to it's differences according to the database used.
For WIKI and IMDB it is slightly different because it could either be in the normal folder, the cropped folder or neither.
        :param record: row from database
        :return: full path
        """

        if 'full_path' in record:
            return record['full_path']
        else:
            print('path error')


def get_deleted_list():

    list_deleted = []

    deleted =  cl_deleted.find({}, {"id": 1})

    for i in deleted:
        list_deleted.append(i['id'])

    return list_deleted


def get_na_list():

    list_services = []

    for service in configuration.list_services:
        list_services += service.process_na_service()

    return list(set(list_services))


def get_data(db_list, object_id):
    for value in db_list:
        cl_db = cl[value.get_name()]["data"]

        records = cl_db.find_one(
            {
                "_id": ObjectId(object_id)
            }
        )

        if records is not None:
            #records['db'] = value.get_name()   #fsanda
            records['db'] = value
            return records


def get_database_name(db_list, object_id):
    for value in db_list:
        cl_db = cl[value.get_name()]["data"]

        records = cl_db.find_one(
            {
                "_id": ObjectId(object_id)
            }
        )

        if records is not None:
            return value.get_name()


def delete_file(object_id):
    cl_deleted.insert(
        {'id': ObjectId(object_id)}
    )


def save_dataset(list_files, gender, min_age, max_age, number_files):

    cl_saved.insert(
        {
            'date': datetime.utcnow(),
            'gender': gender,
            'min_age': min_age,
            'max_age': max_age,
            'number_files': number_files,
            'list_files': list_files
        }
    )