import os
from time import sleep

from bson import ObjectId
from pymongo import MongoClient

import configuration
from alerts.mail_sender import Email
from model import fi_functions

cl = MongoClient()


class Service:
    def __init__(self, name, limit):
        self.name = name
        self.response = None
        self.image_file = None
        self.db = cl[name]["predictions"]
        self.limit = limit
        self.path = None

    def get_path(self):
        return self.path

    def set_path(self, path):
        self.path = path

    def get_limit(self):
        return self.limit

    def get_response(self):
        return self.response

    def get_name(self):
        return self.name

    def get_db(self):
        return self.db

    def service_evaluated(self, subject_id):
        count_records = self.get_db().find({'subject_id': ObjectId(subject_id)}).count()

        if count_records == 0:
            return False
        else:
            return True

    def get_records_stats(self, gender=None):
        """
get list of files
        :return:
        """
        path = self.get_path()

        list_files = fi_functions.iterate_folder(path, gender)

        list_object_files = []

        for i in list_files:
            list_object_files.append(ObjectId(i))

        records = self.get_db().find(
            {
                'subject_id': {'$in': list_object_files}
            }
        )

        print(records.count())

        return records

    def get_records(self):

        records = self.get_db().find()

        return records

    def execute_predictions(self, directory_path):

        counter = 0

        for root, dirs, files in os.walk(directory_path):
            for file in files:

                try:

                    subject_id = os.path.splitext(file)[0]

                    # check if already evaluated

                    if not self.service_evaluated(subject_id):

                        print(self.get_name(), 'in progress...')

                        image_path = os.path.join(root, file)

                        self.set_response(image_path)
                        response = self.get_response()

                        # TODO: control this

                        response['subject_id'] = ObjectId(subject_id)

                        print(response)

                        self.get_db().insert(response)

                        response = None

                    else:
                        counter += 1
                        print(subject_id, 'already evaluated', self.get_name())
                        print(counter)

                except Exception as e:

                    if hasattr(e, 'response'):
                        error_dict = e.response
                    elif hasattr(e, 'response_msg'):
                        error_dict = e.response_msg

                    else:
                        print(e)
                        error_dict = {}

                        # send email alert

                        if False:

                            alert = Email(configuration.smtp_server, configuration.smtp_port, configuration.mail_username,
                                          configuration.alert_namespace)

                            alert.send("ERROR", e)

                    if subject_id is not None:
                        error_dict['subject_id'] = ObjectId(subject_id)

                        #print(str(e))

                        error_dict.update({'error': str(e)})

                        #error_dict['error_object'] = str(e)

                        wait_time = 30

                        if str(e) =='No JSON object could be decoded':
                            print('problem 403..... waiting to resume in {0} seconds'.format(wait_time))
                            sleep(wait_time)
                        else:
                            self.get_db().insert(error_dict)
                    else:
                        error_dict['subject_id'] = 'ERROR'

                    print('error_dict:', error_dict)
                    #return False #change

        return True

    def execute_prediction(self, file_path):
        if file_path is not None:
            response = self.execute_predictions(os.path.dirname(file_path))
        else:
            response = False
            print('file_path does not exist')

        return response

    def get_na_service(self, pipe):

        list_na_services = []

        na_services = self.get_db().find(pipe)

        for i in na_services:
            list_na_services.append(i['subject_id'])

        return list_na_services
