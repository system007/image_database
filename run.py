import io
import configuration
from controller import cn_image
from model.fi_functions import count_images_folder
from view import vw_image
from PIL import Image

images = cn_image.Query(configuration.db_list, configuration.path_directory_creation)


def generate_image_database(gender_database, minimum_age, maximum_age, number_files, path=None):

    if path is None:
        images.delete_folder()
    else:

        # count files in path and match with images demanded print images required

        list_required = count_images_folder(path)

        # generate the demanded files and they can't be repeated

    images.set_number_files(number_files)
    images.set_minimum_age(minimum_age)
    images.set_maximum_age(maximum_age)
    images.set_gender(gender_database)

    run_process = images.process(path)

    # show files

    if True:
        if run_process is not None:

            # always set number, and age before drawing

            if True:
                vw_image.drawGUI(images)

# MAIN


if False:
    gender_eval = "Male"
    min_age_eval = 0
    max_age_eval = 77
    number_images_eval = 65

    if True:
        generate_image_database(gender_eval, min_age_eval, max_age_eval, number_images_eval, configuration.path_directory_creation)

    images.delete_random_databases()

if False:
    images.check_max_file_set(0, 16)

if False:
    savepath = 'D:/temp/'

    bytes = images.get_image("5952d1398ad9561f40c5c225")

    if bytes is not None:
        image = Image.open(io.BytesIO(bytes))
        image.save(savepath)

if True:
    print(count_images_folder(configuration.path_directory_creation, 65))
