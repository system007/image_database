import os

from bson import ObjectId

import configuration

path = configuration.path_directory_creation


def delete_service(service):

    na_list = service.process_na_service()

    for root, dirs, files in os.walk(path):
        for file in files:

            subject_id = ObjectId(os.path.splitext(file)[0])

            if subject_id in na_list:

                file_deleted = os.path.join(root, file)
                os.remove(file_deleted)
                print('file removed', file_deleted)

                # delete from database

                #records = configuration.microsoft_service.get_db().remove({'subject_id': subject_id})
                #print('db removed', records )


def problem():

    na_list = configuration.microsoft_service.process_na_service()

    for root, dirs, files in os.walk(path):
        for file in files:

            subject_id = ObjectId(os.path.splitext(file)[0])

            if subject_id in na_list:

                configuration.microsoft_service.update_how_old_net(subject_id)

                #file_deleted = os.path.join(root, file)
                #os.remove(file_deleted)
                #print('file removed', file_deleted)


if False:
    delete_service(configuration.microsoft_service)


# how-old
if False:

    file_list = []

    for root, dirs, files in os.walk(path):
        for file in files:

            subject_id = os.path.splitext(file)[0]
            file_list.append(subject_id)


#problem()

if False:
    configuration.microsoft_service.update_how_old_net()

