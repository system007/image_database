import os

from feret.feret import process_bz2_feret, FERET

folder_output = 'D:/Documents/research/evaluation/feret_frontal'
subjects_file = 'subjects.xml'
recordings_file = 'recordings.xml'

disk1 = 'dvd1'
disk2 = 'dvd2'

f = FERET()

if False:

    # 1) insert recordings and subject from disk 1 to mongodb
    ground_truth_disk1 = 'D:/Documents/research/feret/colorferet/' + disk1 + '/data/ground_truths/xml/'

    f.insert_recordings(os.path.join(ground_truth_disk1, recordings_file), 1)
    f.insert_subject(os.path.join(ground_truth_disk1, subjects_file), 1)

if False:
    # 2) insert recordings and subject from disk 1 to mongodb

    disk2_folder_path = 'D:/Documents/research/feret/colorferet/' + disk2 + '/data/ground_truths/xml/'

    for root, subFolders, files in os.walk(disk2_folder_path, topdown=True):
        for f in files:

            current_directory  = root[-5:]
            file_without_extension = os.path.splitext(f)[0]

            if f.endswith('.xml') and current_directory == file_without_extension:
                f.insert_subject(os.path.join(root, f),2)
            elif f.endswith('.xml'):
                f.insert_recordings(os.path.join(root, f),2)


if False:
    # 3) Unzip *ALL files

    # Disk 1

    root_folder = 'D:/Documents/research/feret/colorferet/dvd1/data/images/'

    for root, subFolders, files in os.walk(root_folder, topdown=True):
        for f in files:
            if f.endswith('.bz2'):
                process_bz2_feret(os.path.join(root,f), folder_output)

    # Disk 2

    root_folder = 'D:/Documents/research/feret/colorferet/dvd2/data/images/'

    for root, subFolders, files in os.walk(root_folder, topdown=True):
        for f in files:
            if f.endswith('.bz2'):
                process_bz2_feret(os.path.join(root, f), folder_output)

if False:
    # 3) Unzip ONLY frontal image files

    # Disk 1

    root_folder = 'D:/Documents/research/feret/colorferet/dvd1/data/images/'

    for root, subFolders, files in os.walk(root_folder, topdown=True):
        for f in files:
            if f.endswith('.bz2') and '_fa' in f:
                process_bz2_feret(os.path.join(root, f), folder_output)
                break

    # Disk 2

    root_folder = 'D:/Documents/research/feret/colorferet/dvd2/data/images/'

    for root, subFolders, files in os.walk(root_folder, topdown=True):
        for f in files:
            if f.endswith('.bz2') and '_fa' in f:
                process_bz2_feret(os.path.join(root, f), folder_output)

if True:
    # 4) Retrieve ages

    f.get_age_frontal_images()
