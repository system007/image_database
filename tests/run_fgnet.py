import os
import shutil

from fgnet import fgnet
from fgnet.fgnet import get_age, get_gender, count_files

root_dir = "D:/Documents/research/fg-net/Images"
copy_dir = "D:/Documents/research/evaluation/images"


if False:

    # 1) Classify to folders according to age and gender

    list = os.listdir(root_dir) # dir is your directory path
    number_files = len(list)
    print(number_files)

    if os.path.exists(copy_dir):
        shutil.rmtree(copy_dir)

    os.makedirs(copy_dir)

    for filename in os.listdir(root_dir):
        if filename.lower().endswith(".jpg"):

            source_file = os.path.join(root_dir, filename)
            print(source_file)

            folder = get_age(filename)

            gender = 'Male' if get_gender(filename) == 'M' else 'Female'

            # count files

            copy_directory = os.path.join(copy_dir, str(folder))

            final_directory = os.path.join(copy_directory,gender)

            print(final_directory)

            destination_file = os.path.join(final_directory, filename)

            if not os.path.exists(final_directory):
                os.makedirs(final_directory)

            count = len(os.listdir(final_directory))

            if count < 20:
                shutil.copy2( source_file, destination_file)

if False:
    # 2) Count ages by gender

    count_files(root_dir)

if True:
    # 3) Import images to database

    fgnet.upload_database(root_dir)
