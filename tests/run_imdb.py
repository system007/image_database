from imdb.imdb import upload_db_from_csv
from model.db_image import Database

imdb_crop_root_path = 'D:/Download/data-vision/imdb_crop/'
imdb_root_path = 'D:/Download/data-vision/imdb/'
wiki_root_path = 'D:/Download/data-vision/wiki.tar/wiki/'
wiki_crop_root_path = 'D:/Download/data-vision/wiki_crop/'
imdb_csv_path = 'D:/Download/data-vision/imdb_meta/imdb/imdb.mat'

if False:
    #upload database
    upload_db_from_csv(imdb_csv_path)

imdb = Database("IMDB")

imdb.set_crop_path(imdb_crop_root_path)
imdb.set_root_path(imdb_root_path)

print(imdb.count_images(10, 0))

wiki = Database("WIKI")

wiki.set_crop_path(wiki_crop_root_path)
wiki.set_root_path(wiki_root_path)

print(wiki.count_images(10, 0))
