import os

from Tkinter import *
from PIL import Image, ImageTk, ImageFile

import configuration
from controller import cn_evaluation
from model import fi_functions

ImageFile.LOAD_TRUNCATED_IMAGES = True

buttons = []


class ImageDecorator:

    def __init__(self, decorated):

        self.image_size = 70

        self.decorated = decorated
        self.button_save = None
        self.button_predict = None
        self.button_stats = None

    def set_button_save(self, button_save):
        self.button_save = button_save

    def get_button_save(self):
        return self.button_save

    def set_button_predict(self, button_predict):
        self.button_predict = button_predict

    def get_button_predict(self):
        return self.button_predict

    def set_button_stats(self, button_stats):
        self.button_stats = button_stats

    def get_button_stats(self):
        return self.button_stats

    def get_image_size(self):
        return self.image_size

    def btn_action(self, index, text):

        image_path = self.decorated.replace_image(text)

        if image_path is not None:
            im = Image.open(image_path)

            resize = im.resize((self.get_image_size(), self.get_image_size()), Image.ANTIALIAS)

            img = ImageTk.PhotoImage(resize)

            buttons[index].image = img

            buttons[index].config(image=img)
            buttons[index].config(command=lambda path=image_path, index=index: self.btn_action(index, path))

        else:
            buttons[index].config(state="disabled")

    def draw(self, frame):
        index = 0
        row_counter = 0
        column_counter = 0

        if True:

            for root, subFolders, files in os.walk(self.decorated.get_path_directory_creation(), topdown=True):

                if fi_functions.check_integer_list(subFolders):
                    subFolders.sort(key=float)

                for f in files:
                    if f.lower().endswith(".jpg"):

                        im = Image.open(os.path.join(root, f))

                        resize = im.resize((self.get_image_size(), self.get_image_size()), Image.ANTIALIAS)

                        img = ImageTk.PhotoImage(resize)

                        path = os.path.join(root, f)

                        button = Button(
                            frame,
                            image=img,
                            command=lambda image_path=path, button_index=index: self.btn_action(button_index, image_path)
                        )

                        button.image = img
                        button.grid(row=row_counter, column=column_counter)

                        index += 1

                        buttons.append(button)

                        column_counter += 1
                        if column_counter == self.decorated.get_number_files():
                            age = self.decorated.get_age(f)

                            label_age = Label(frame, text=age)

                            label_age.grid(row=row_counter, column=column_counter + 1)
                            column_counter = 0
                            row_counter += 1


    def create_buttons(self, canvas):

        button_save = Button(canvas, text="Save", command=self.btn_save)

        self.set_button_save(button_save)

        button_save.pack(side="left")

        button_predict = Button(canvas, text="Predict", command=self.btn_predict)

        self.set_button_predict(button_predict)

        button_predict.pack(side="left")

        button_stats = Button(canvas, text="Stats", command=self.btn_stats, state='disabled')

        self.set_button_stats(button_stats)

        button_stats.pack(side="left")

    def btn_save(self):

        self.get_button_save().config(state="disabled")

        self.decorated.save_database()

        print('saved')

    def btn_predict(self):
        print('predicting')
        self.get_button_predict().config(state='disabled')
        cn_evaluation.predict_services(self.decorated.get_path_directory_creation(), configuration.list_services)
        self.get_button_stats().config(state='active')
        print('done')

    def btn_stats(self):
        print('stats')
        cn_evaluation.print_stats(self.decorated.get_path_directory_creation())


class AutoScrollbar(Scrollbar):
    # a scrollbar that hides itself if it's not needed.  only
    # works if you use the grid geometry manager.
    def set(self, lo, hi):
        if float(lo) <= 0.0 and float(hi) >= 1.0:
            # grid_remove is currently missing from Tkinter!
            self.tk.call("grid", "remove", self)
        else:
            self.grid()
        Scrollbar.set(self, lo, hi)
    def pack(self, **kw):
        raise TclError, "cannot use pack with this widget"
    def place(self, **kw):
        raise TclError, "cannot use place with this widget"

def drawGUI(images):

    root = Tk()

    root.state('zoomed')

    vscrollbar = AutoScrollbar(root)
    vscrollbar.grid(row=0, column=1, sticky=N+S)
    hscrollbar = AutoScrollbar(root, orient=HORIZONTAL)
    hscrollbar.grid(row=1, column=0, sticky=E+W)

    canvas = Canvas(root,
                    yscrollcommand=vscrollbar.set,
                    xscrollcommand=hscrollbar.set)
    canvas.grid(row=0, column=0, sticky=N+S+E+W)

    vscrollbar.config(command=canvas.yview)
    hscrollbar.config(command=canvas.xview)

    # make the canvas expandable
    root.grid_rowconfigure(0, weight=1)
    root.grid_columnconfigure(0, weight=1)

    # create canvas contents

    frame = Frame(canvas)
    frame.rowconfigure(1, weight=1)
    frame.columnconfigure(1, weight=1)

    #here the logic

    image_decorator = ImageDecorator(images)

    image_decorator.draw(frame)

    frameButtons = Frame(canvas)

    image_decorator.create_buttons(frameButtons)

    canvas.create_window(0, 0, anchor=NW, window=frame)

    frameButtons.pack(side="bottom")

    frame.update_idletasks()

    canvas.config(scrollregion=canvas.bbox("all"))

    root.mainloop()
