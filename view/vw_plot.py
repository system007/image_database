import plotly.plotly as py
import plotly.graph_objs as go

import configuration
from controller import cn_evaluation

py.plotly.tools.set_credentials_file(
    username= configuration.plotly_credentials['username'],
    api_key=configuration.plotly_credentials['api_key']
)

path = configuration.path_directory_creation
#path_directory_creation = 'D:/Documents/research/evaluation/dataset_paper/'
#path = path_directory_creation


def plot_graph(min_value, max_value, title_text, gender=None):

    x_axis = [i for i in range(min_value, max_value+1)]

    data = []

    # Create and style traces
    trace0 = go.Scatter(
        x=x_axis,
        y=x_axis,
        name='100%',
        showlegend=False,
        line=dict(
            color='rgb(0, 0, 0)',
            width=1,
            dash='dot'
        )
    )

    data.append(trace0)

    attributes = [
        {
            "id": 1,
            "colors": {'R': 44, 'G': 52, 'B': 68},        # Kairos (GREY)
            "symbol": "diamond-tall"
        },
        {
            "id": 2,
            "colors": {'R': 255, 'G': 153, 'B': 0},      # Amazon (YELLOW)
            "symbol": "triangle-up"
        },
        {
            "id": 3,
            "colors": {'R': 255, 'G': 0, 'B': 0},        # DEX
            "symbol": "cross"
        },
        {
            "id": 4,
            "colors": {'R': 124, 'G': 187, 'B': 0},       # Microsoft (2nd green colour)
            "symbol": "square"
        },
        {
            "id": 5,
            "colors": {'R': 70, 'G': 107, 'B': 176},     # IBM (BLUE)
            "symbol": "circle"
        }
        ,
        {
            "id": 6,
            "colors": {'R': 70, 'G': 107, 'B': 176},  # HOW-OLD (BLUE)
            "symbol": "circle"
        }

    ]

    counter = 0

    for service in configuration.list_services:

        R = attributes[counter]["colors"]['R']
        G = attributes[counter]["colors"]['G']
        B = attributes[counter]["colors"]['B']

        # get mean of all the values for the purpose of plotting

        stats = cn_evaluation.get_stats(service, path, configuration.db_list, gender)

        y_axis_service = cn_evaluation.get_plot_data(stats).values()

        colorString = 'rgb({0},{1},{2})'.format(R, G, B)

        #mae = cn_evaluation.calculate_mae(y_axis_service,x_axis)

        mae = cn_evaluation.calculate_mae(stats)

        legend_service = service.get_name()

        legend_name = normalize_legend(legend_service)

        trace_service = go.Scatter(

            x=x_axis,
            y=y_axis_service,
            name=legend_name,
            mode='lines+markers',
            marker=dict(
                symbol=attributes[counter]["symbol"]
            ),
            text="MAE={0}".format(mae),
            line=dict(
                color=colorString,
                width=1,
                shape='spline',
            )
        )

        data.append(trace_service)
        counter += 1

    if gender is not None:
        format_gender = 'for ' + gender
    else:
        gender = ''
        format_gender = ''

    # Edit the layout
    layout = dict(
        title=title_text + 'Average Estimated Age VS Real Age {0}'.format(format_gender),
        autosize=True,
        font=dict(size=20),
        margin=dict(
            b=400),
        #width=500,
        #height='5%',

        legend=dict(
        #    x=.009,
        #    y=.995,
            borderwidth=1
        ),

        xaxis=dict(
            title='Age',
            showline=True
        ),
        yaxis=dict(
            title='Prediction',
            showline=True
        ),
)

    fig = dict(data=data, layout=layout)
    py.plot(fig, filename='linear-plot' + '-' + title_text + gender)


def normalize_legend(legend_name):
    if legend_name == 'SRV_AMAZON':
        return 'AWS'
    elif legend_name == 'SRV_MICROSOFT':
        return 'Azure'
    elif legend_name == 'SRV_KAIROS':
        return 'Kairos'
    elif legend_name == 'SRV_DEX':
        return 'DEX'
    elif legend_name == 'SRV_HOW-OLD':
        return 'HOW-OLD'


plot_graph(0, 30, 'test', 'male')