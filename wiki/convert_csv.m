I created a script using octave that can convert the structure of the DEX mat file to CSV so I can consume it
----------------------------------------------------------------------------------------------------------
1) load mat file to octave

load('D:\Documents\research\wiki\wiki.mat')

----------------------------------------------------------------------------------------------------------

2) Use fieldnames(wiki) in Octave to get the fields

----------------------------------------------------------------------------------------------------------

l = length(wiki.full_path)

try

  fid = fopen ('wiki.csv', 'a');

  fprintf(fid, '%s,%s,%s,%s,%s,%s\n', 'full_path', 'name', 'age', 'gender', 'face_score', 'second_face_score')

  for i=1:l

    full_path = wiki.full_path(i){1}
    dob = wiki.dob(i)
    [age,~]=datevec(datenum(wiki.photo_taken(i),7,1)-wiki.dob(i));
    gender = wiki.gender(i)
    name = wiki.name(i){1}
    face_score = wiki.face_score(i)
    second_face_score = wiki.second_face_score(i)

    fprintf(fid, '%s,%s,%d,%d,%d,%d\n', full_path, name, age, gender, face_score, second_face_score);

  end
  fclose(fid)
catch ex
  disp(ex)
  disp(i)
  fclose(fid)
end