import csv
import os
import random

import configuration
from controller import cn_image
from model import db_service
from model.db_image import Database, get_na_list


class WIKI(Database):
    def __init__(self):
        Database.__init__(self, "WIKI")
        self.set_crop_path(configuration.wiki_crop_root_path)

    def set_image_path(self, record):
        #full_path_url = self.manage_paths(record['full_path'])
        full_path_url = self.manage_paths(record)
        self.image_path = full_path_url

    def upload_db_from_txt(self, wiki_file_path):
        with open(wiki_file_path) as txtfile:

            csv.register_dialect('piper', delimiter='|', quoting=csv.QUOTE_NONE)

            reader = csv.DictReader(txtfile, dialect='piper')
            for row in reader:
                self.get_db().insert(row)

    def get_filtered_images(self, filtered_list):

        #list_deleted = self.get_deleted_list()
        #list_na_service = get_na_list()

        #return list(set(list_deleted + list_na_service))

        return list(set(filtered_list))

    def get_record(self, age, gender, filtered_images):

        #list_deleted = self.get_deleted_list()
        list_processed = self.get_processed_list()
        #list_na_service = get_na_list()

        #filtered_list = list_deleted + list_na_service

        pipe = {
            'age': str(age),
            'gender': str(gender),
            'second_face_score': 'NaN',
            #'_id': {"$nin": list_deleted},
            #'_id': {"$nin": filtered_list},
            '_id': {"$nin": filtered_images},
            'full_path': {"$nin": list_processed},
            'face_score': {"$lt": '0'}
        }

        return self.get_db().find(pipe)

    def count_images(self, age, gender, filtered_images):

        images = self.get_record(age, gender, filtered_images)

        images_dictionary = []

        for row in images:

            full_path_url = self.manage_paths(row)

            row['full_path_url'] = full_path_url

            images_dictionary.append(row)

        return len(images_dictionary)

    def manage_paths(self, record):

        file_name = record['full_path']

        image_path_a = os.path.join(self.get_crop_path(), file_name)
        image_path_b = os.path.join(self.get_root_path(), file_name)

        full_path_url = None

        if os.path.exists(image_path_a):
            full_path_url = image_path_a
        elif os.path.exists(image_path_b):
            full_path_url = image_path_b
        else:
            print('image not in path', file_name)
            # delete from db
            record_id = record['_id']

            cn_image.delete_record(record_id)

        return full_path_url

    def get_random_image(self, age, gender, filtered_images):

        images = self.get_record(age, gender, filtered_images)

        images_dictionary = []

        for row in images:
            file_name = row['full_path']

            #full_path_url = self.manage_paths(file_name)
            full_path_url = self.manage_paths(row)

            row['full_path_url'] = full_path_url

            images_dictionary.append(row)

        if len(images_dictionary) > 0:
            random_image = random.choice(images_dictionary)

            id_random = random_image['full_path']

            self.get_random().insert(
                {'id': id_random}
            )

            return random_image
