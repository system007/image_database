import random
from model.db_image import Database


class YAHOO(Database):
    def __init__(self):
        Database.__init__(self, "YAHOO")

    def resume_upload(self,root_dir):

        #latest_record = self.get_db().find().limit(1).sort('Line number',-1)

        latest_line_number = 15902317

        yahoo_file = root_dir

        with open(yahoo_file) as infile:
            for line in infile:
                line_list = line.split("\t")

                record_dict = {

                    'Line number': line_list[0],
                    'identifier': line_list[1],
                    'hash': line_list[2],
                    'User NSID': line_list[3],
                    'User nickname': line_list[4],
                    'Date taken': line_list[5],
                    'Date uploaded': line_list[6],
                    'Capture device': line_list[7],
                    'Title': line_list[8],
                    'Description': line_list[9],
                    'User tags': separate_tag(',', line_list[10]),
                    'Machine tags': separate_tag(',', line_list[11]),
                    'Longitude': line_list[12],
                    'Latitude': line_list[13],
                    'Accuracy of longitude and latitude': line_list[14],
                    # coordinates(1 = world level accuracy, ..., 16 = street level accuracy)'
                    'page URL': line_list[15],
                    'download URL': line_list[16],
                    'License name': line_list[17],
                    'License URL': line_list[18],
                    'server identifier': line_list[19],
                    'farm identifier': line_list[20],
                    'secret': line_list[21],
                    'secret original': line_list[22],
                    'Extension of the original photo': line_list[23],
                    'marker': line_list[24]
                    # (0 = photo, 1 = video)
                }

                print(record_dict['Line number'])

                if int(record_dict['Line number'])>latest_line_number:
                    self.get_db().insert(record_dict)
                    print(record_dict)

    def upload_database(self, root_dir):

        yahoo_file = root_dir

        with open(yahoo_file) as infile:
            for line in infile:
                line_list = line.split("\t")

                record_dict = {

                    'Line number': line_list[0],
                    'identifier': line_list[1],
                    'hash': line_list[2],
                    'User NSID': line_list[3],
                    'User nickname': line_list[4],
                    'Date taken': line_list[5],
                    'Date uploaded': line_list[6],
                    'Capture device': line_list[7],
                    'Title': line_list[8],
                    'Description': line_list[9],
                    'User tags': separate_tag(',', line_list[10]),
                    'Machine tags': separate_tag(',', line_list[11]),
                    'Longitude': line_list[12],
                    'Latitude': line_list[13],
                    'Accuracy of longitude and latitude': line_list[14],
                    # coordinates(1 = world level accuracy, ..., 16 = street level accuracy)'
                    'page URL': line_list[15],
                    'download URL': line_list[16],
                    'License name': line_list[17],
                    'License URL': line_list[18],
                    'server identifier': line_list[19],
                    'farm identifier': line_list[20],
                    'secret': line_list[21],
                    'secret original': line_list[22],
                    'Extension of the original photo': line_list[23],
                    'marker': line_list[24]
                    # (0 = photo, 1 = video)
                }

                print(record_dict)

                self.get_db().insert(record_dict)

    def get_data_for_adience(self, user_id, identifier, tags):

        cursor = self.get_db().find(
            {
                'User NSID': user_id,
                'identifier': identifier,
                #'User tags': {'$in':  tags}
            }
        )

        return cursor

    def filter_data(self, title):
        cursor = self.get_db().find(
            {
                'Title': '/'+title+'/'
            }
        )

    def set_image_path(self, record):
        # full_path_url = self.manage_paths(record['full_path'])
        full_path_url = self.manage_paths(record)
        self.image_path = full_path_url

    def get_record(self, age, gender):

        #list_deleted = self.get_deleted_list()
        #list_processed = self.get_processed_list()

        pipe = {
            'age': str(age),
            'gender': str(gender),
            'second_face_score': 'NaN',
            #'_id': {"$nin": list_deleted},
            #'full_path': {"$nin": list_processed}
        }

        return self.get_db().find(pipe)

    def count_images(self, age, gender):

        images = self.get_record(age, gender)

        images_dictionary = []

        for row in images:
            full_path_url = self.manage_paths(row)

            row['full_path_url'] = full_path_url

            images_dictionary.append(row)

        return len(images_dictionary)

    def get_random_image(self, age, gender, filtered_images):

        images = self.get_record(age, gender, filtered_images)

        images_dictionary = []

        for row in images:
            file_name = row['full_path']

            full_path_url = self.manage_paths(row)

            row['full_path_url'] = full_path_url

            images_dictionary.append(row)

        if len(images_dictionary) > 0:
            random_image = random.choice(images_dictionary)

            id_random = random_image['full_path']

            self.get_random().insert(
                {'id': id_random}
            )

            return random_image


def separate_tag(delimiter, text):
    if delimiter in text:
        return str(text).split(delimiter)
    return text
